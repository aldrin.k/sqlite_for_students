import sqlite3
conn = sqlite3.connect('Student.db')
cur = conn.cursor()

cur.execute(""" CREATE TABLE studentdetails (
            first_name text,
            last_name text,
            email text
)
""")
conn.commit()
conn.close()

# Datatypes in SQLite:
# NULL
# INTEGER
# REAL
# TEXT
# BLOB