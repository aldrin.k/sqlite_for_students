import sqlite3
conn = sqlite3.connect('Employee.db')
cur = conn.cursor()

# cur.execute("SELECT Designation FROM employeeDetails") -> One Column
# cur.execute("SELECT * FROM employeeDetails WHERE Designation == 'Developer'") -> Only developers details
# cur.execute("SELECT Name,empID FROM employeeDetails WHERE Designation == 'Developer'")
# cur.execute("SELECT Name FROM employeeDetails WHERE Age >= 25 AND Name LIKE 'S%' ")
# cur.execute("SELECT Name FROM employeeDetails WHERE Designation LIKE '%er' ")
# cur.execute("SELECT Name FROM employeeDetails ORDER BY Salary")
cur.execute("SELECT Name,empid FROM employeeDetails ORDER BY Salary DESC ")
# cur.execute("SELECT Name FROM employeeDetails ORDER BY Salary DESC LIMIT 5")
# cur.execute("SELECT Name FROM employeeDetails WHERE SALARY = (select max(SALARY) FROM employeeDetails)")



print(cur.fetchall())

# print('SQL command is successfully executed')
conn.commit()
conn.close()