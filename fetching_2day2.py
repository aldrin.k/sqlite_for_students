import sqlite3
conn = sqlite3.connect('Student.db')
cur = conn.cursor()

# Using Where clause to get specific values
# cur.execute("SELECT * FROM studentdetails WHERE last_name = 'Pratap Singh' ")

# Using where clause to get only rowid and email
cur.execute("SELECT rowid, email FROM studentdetails WHERE first_name = 'Tarun'")

for row in cur.fetchall():
    print(row)

conn.commit()
conn.close()
