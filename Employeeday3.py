import sqlite3
conn = sqlite3.connect('Employee.db')
cur = conn.cursor()

# cur.execute("""
#             CREATE TABLE employeeDetails (
#                 empID INTEGER, Name TEXT, Designation TEXT, DOJ TEXT, Salary INTEGER, Age Integer)
# """)

datas =  [
    (1234, 'Dinesh', 'Developer', '10.02.2021', 450000, 22),
    (1235, 'Rana', 'Developer', '10.02.2021', 550000, 23),
    (1236, 'Jaswanth', 'Sr Developer', '6.07.2020', 650000, 24),
    (1238, 'Pradeep', 'HOD', '3.01.2019', 630000, 25),
    (1245, 'Govind', 'Tester', '15.11.2021', 430000, 21),
    (1255, 'Pavan', 'Developer', '3.01.2021', 750000, 25),
    (1217, 'Tarun', 'Sr Developer', '30.12.2020', 800000, 28),
    (1256, 'Omkar', 'Manager', '11.05.2020', 850000, 30),
    (1231, 'Ravali', 'Sr Manager', '15.07.2019', 950000, 28),
    (1230, 'Sharukh', 'Sr DevOps', '14.07.2021', 820000, 27),
    (1257, 'Sravani', 'HR', '10.10.2020', 650000, 25)                        
    ]

cur.executemany("INSERT INTO employeeDetails VALUES (?, ?, ?, ?, ?, ?)",datas)

print('SQL command is successfully executed')
conn.commit()
conn.close()