import sqlite3
conn = sqlite3.connect('Student.db')
cur = conn.cursor()

datas = [
        ('Jashwanth', 'Proxy', 'Proxy@attendance.com'),
        ('Govind', 'ORR', 'ORR@gmail.com'),
        ('Suraj', 'Kondalwade', 'Suraj@chand.com')
]

cur.executemany("INSERT INTO studentdetails VALUES(?, ?, ?)", datas)
print('SQL command is successfully executed')
conn.commit()
conn.close()

# Name, email, DOB, phonenumber,