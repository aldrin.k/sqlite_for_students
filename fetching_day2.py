import sqlite3
conn = sqlite3.connect('Student.db')
cur = conn.cursor()

# cur.execute("SELECT * FROM studentdetails")
cur.execute("SELECT rowid, * FROM studentdetails") # Getting rowid along with all datas

# print(cur.fetchall())
for row in cur.fetchall():
    print(row)
# print(cur.fetchone())
# print(cur.fetchmany(5))

# for row in cur.fetchall():
#     # if row[0][0] == 'R':
#     print(f"{row[0]} {row[1]} {row[2]}")
conn.commit()
conn.close()
