# Fetching one column 

import sqlite3
connection = sqlite3.connect('student.db')
cursor = connection.cursor()

# Creating a table with name StudentDetails with column name ID, name, DOB.

cursor.execute("SELECT name FROM sqlite_master")
print('Fetched one table', cursor.fetchone())
cursor.close()
connection.close()