import sqlite3
conn = sqlite3.connect('Employee.db')
cur = conn.cursor()

# cur.execute("UPDATE employeeDetails SET Designation = 'Sr Developer' WHERE NAME == 'Pavan' ")
# cur.execute("UPDATE employeeDetails SET Designation = 'Sr Developer' WHERE Designation = 'Developer'")
# cur.execute("SELECT * FROM employeeDetails WHERE empID = 1234 OR empID = 1235 OR empID = 1255")
cur.execute("UPDATE employeeDetails SET Salary = Salary + (Salary*0.11) WHERE empID = 1234 OR empID = 1235 OR empID = 1255")
# print(cur.fetchall())
# print('SQL command is successfully executed')
conn.commit()
conn.close()