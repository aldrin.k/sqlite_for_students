# Connection and checking current version of SQLite 

import sqlite3
connection = sqlite3.connect('student.db')
cursor = connection.cursor()

# SQL query for version 
query = 'select sqlite_version();' 

#Execute using execute method()
cursor.execute(query)

# fetchall() is used to fetch the result from the server
result = cursor.fetchall()

print('The version is {}'.format(result))

# Close the cursor and the connection
cursor.close()
connection.close()