#
# Types of SQL commands:
#
### <b>1.DDL Data Definition Language(DDL)</b>: It changes a table’s structure by adding, deleting and altering its contents. Its changes are auto-committed(all changes are automatically permanently saved in the database). Some commands that are a part of DDL are:


<i>CREATE: Used to create a new table in the database.CREATE: Used to create a new table in the database.

Example:

CREATE TABLE STUDENT(Name VARCHAR2(20), Email VARCHAR2(100), DOB DATE);  

ALTER: Used to alter contents of a table by adding some new column or attribu

Example:

ALTER TABLE STUDENT ADD(ADDRESS VARCHAR2(20));  
ALTER TABLE STUDENT MODIFY (ADDRESS VARCHAR2(20));  

DROP: Used to delete the structure and record stored in the table.

Example:

DROP TABLE STUDENT;  

TRUNCATE: Used to delete all the rows from the table, and free up the space in the table.

Example:

TRUNCATE TABLE STUDENT;
</i>

### 2. <b>Data Manipulation Language(DML):</b> It is used for modifying a database, and is responsible for any form of change in a database. These commands are not auto-committed, i.e all changes are not automatically saved in the database. Some commands that are a part of DML are:

<i>
INSERT: Used to insert data in the row of a table.

Example:

INSERT INTO STUDENT (Name, Subject) VALUES ("Scaler", "DSA");  

<b>In the above example, we insert the values “Scaler” and “DSA” in the columns Name and Subject in the STUDENT table.</b>

UPDATE: Used to update value of a table’s column.

Example:

UPDATE STUDENT   
SET User_Name = 'Interviewbit'    
WHERE Student_Id = '2'  

<b>In the above example, we update the name of the student, whose Student_ID is 2, to the User_Name = “Interviewbit”.</b>

DELETE: Used to delete one or more rows in a table.

Example:

DELETE FROM STUDENT 
WHERE Name = "Scaler"; 

<b> In the above example, the query deletes the row where the Name of the student is “Scaler” from the STUDENT table.</b></i>

### <b>3. Data Control Language(DCL): </b> These commands are used to grant and take back access/authority (revoke) from any database user. Some commands that are a part of DCL are:

<i>Grant: Used to grant a user access privileges to a database.

Example:

GRANT SELECT, UPDATE ON TABLE_1 TO USER_1, USER_2;  

<b>In the above example, we grant the rights to SELECT and UPDATE data from the table TABLE_1 to users - USER_1 and USER_2.

Revoke: Used to revoke the permissions from an user.</b>

Example:

REVOKE SELECT, UPDATE ON TABLE_1 FROM USER_1, USER_2;

<b>In the above example we revoke the rights to SELECT and UPDATE data from the table TABLE_1 from the users- USER_1 and USER_2.</b>

### <b>4. Transaction Control Language:</b> These commands can be used only with DML commands in conjunction and belong to the category of auto-committed commands. Some commands that are a part of TCL are:


<i>COMMIT: Saves all the transactions made on a database.

Example:

DELETE FROM STUDENTS
WHERE AGE = 16;  
COMMIT;  
<b>In the above database, we delete the row where AGE of the students is 16, and then save this change to the database using COMMIT.</b>

ROLLBACK: It is used to undo transactions which are not yet been saved.

Example:

DELETE FROM STUDENTS 
WHERE AGE = 16;  
ROLLBACK;  
<b>By using ROLLBACK in the above example, we can undo the deletion we performed in the previous line of code, because the changes are not committed yet.</b>

SAVEPOINT: Used to roll transaction back to a certain point without having to roll back the entirity of the transaction.

Example:

SAVEPOINT SAVED;
DELETE FROM STUDENTS 
WHERE AGE = 16;  
ROLLBACK TO SAVED;

<b>In the above example, we have created a savepoint just before performing the delete operation in the table, and then we can return to that savepoint using the ROLLBACK TO command.</b>

### <b>5. Data Query Language:</b> It is used to fetch some data from a database. The command belonging to this category is:

<i>SELECT: It is used to retrieve selected data based on some conditions which are described using the WHERE clause. It is to be noted that the WHERE clause is also optional to be used here and can be used depending on the user’s needs.

Example: With WHERE clause,

SELECT Name  
FROM Student  
WHERE age >= 18;  

Example: Without WHERE clause,

SELECT Name  
FROM Student 

<b>In the first example, we will only select those names in the Student table, whose corresponding age is greater than 17. In the 2nd example, we will select all the names from the Student table.</b>

#
#
# Curd Operations
#
<b>Crud Operations in SQL
CRUD is an abbreviation for Create, Read, Update and Delete. These 4 operations comprise the most basic database operations. The relevant commands for these 4 operations in SQL are:</b>

<b><pre>
Create: INSERT
Read: SELECT
Update: UPDATE
Delete: DELETE
</b></pre>

#
#
# Important commands for SQL
#
<pre>
Some of The Most Important SQL Commands
SELECT - extracts data from a database
UPDATE - updates data in a database
DELETE - deletes data from a database
INSERT INTO - inserts new data into a database
CREATE DATABASE - creates a new database
ALTER DATABASE - modifies a database
CREATE TABLE - creates a new table
ALTER TABLE - modifies a table
DROP TABLE - deletes a table
CREATE INDEX - creates an index (search key)
DROP INDEX - deletes an index
</pre>