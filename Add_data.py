# Connecting and adding a table

import sqlite3
connection = sqlite3.connect('student.db')
cursor = connection.cursor()

# Creating a table with name StudentDetails with column name ID, name, DOB.

cursor.execute("CREATE TABLE StudentsDetail(ID, name, DOB)")

cursor.close()
connection.close()